programa {
	funcao inicio() {
		const real KM_POR_LITRO = 12.0
		real tempo_gasto, velocidade_media, distancia, litros_usados

		escreva("Digite o tempo gasto na viagem: ")
		leia(tempo_gasto)
		escreva("Digite a velocidade média: ")
		leia(velocidade_media)

		distancia = velocidade_media * tempo_gasto

		litros_usados = distancia / KM_POR_LITRO

		escreva("Você gastou ", litros_usados, " litros de gasolina!")
	}
}