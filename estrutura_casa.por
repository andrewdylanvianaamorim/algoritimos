programa {
	funcao inicio() {
		escreva("Olá, tudo bem? Sou a Iris, a assistente virtual")
		escreva(" do Hospital de Olhos Francisco Vilar, Muito prazer")

		escreva("\nSobre Qual Assunt posso ajudar?\n")

		escreva("1 - Agendamento\n")
		escreva("2 - Cirurgia\n")
		escreva("3 - Financeiro\n")
		escreva("4 - Lente de Contato\n")
		escreva("5 - Informações e outros assuntos\n")

		escreva("Digite sua opção: ")

		inteiro opcao
		leia(opcao)

		escreva("você escolheu ")
	
		para(opcao) {
			caso 1:
				escreva("a opção para os agendamentos.")
				pare
			case 2:
				escreva("a opção para tratar das cirurgias.")
				pare
			case 3:
				escreva("a opção para tratar de assuntos financeiros.")
				pare
			case 4:
				escreva("a opção para tratar das lentes de contato")
				pare
			case 5:
				escreva("");
		}
	}
}
