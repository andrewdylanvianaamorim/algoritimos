programa {
	funcao inicio() {
		const real LITROS_POR_METRO_CUBICO = 1000.0
		real GRAMAS_POR_LITRO = 1000.0 / 14.0
		real metros_cubicos, litros, totalDeGramas
		

		escreva("Digite o número de metros cúbicos da picina: ") 
		leia(metros_cubicos)

		litros = metros_cubicos * LITROS_POR_METRO_CUBICO

		totalDeGramas = litros * GRAMAS_POR_LITRO

		escreva("Você deve colocar ", totalDeGramas, " gramas de cloro")
	}
}
