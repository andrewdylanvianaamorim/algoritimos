programa {
    funcao inteiro Potencia(inteiro base, inteiro expoente) {
	inteiro resultado = base
	para(inteiro c = 1; c < expoente; c++) resultado *= base
	retorne resultado
    }

    funcao inicio() {
	escreva(Potencia(5,2))
    }
}
