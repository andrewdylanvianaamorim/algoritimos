programa {
	funcao inicio() {
		cadeia nome1, nome2, nome_mais_pesada, nome_mais_alta
		real peso1, peso2, altura1, altura2, maior_peso, maior_altura


		escreva("===== 1º PESSOA =====\n")
		escreva("Nome: ")
		leia(nome1)
		escreva("Peso: ")
		leia(peso1)
		escreva("Altura: ")
		leia(altura1)
		escreva("=====================\n")
		
		escreva("===== 2º PESSOA =====\n")
		escreva("Nome: ")
		leia(nome2)
		escreva("Peso: ")
		leia(peso2)
		escreva("Altura: ")
		leia(altura2)
		escreva("=====================\n")


		se (peso1 > peso2) {
			maior_peso = peso1
			nome_mais_pesada = nome1
		} senao {
			maior_peso = peso2
			nome_mais_pesada = nome2
		}
		
		se (altura1 > altura2) {
			maior_altura = altura1
			nome_mais_alta = nome1
		} senao {
			maior_altura = altura2
			nome_mais_alta = nome2
		}

		escreva("==== RESULTADO ====\n")
		
		se (peso1 == peso2) {
			escreva("Os dois tem o mesmo peso\n")
		} senao {
			escreva("Com ", maior_peso, "kg o mais pesado é: ", nome_mais_pesada, "\n")	
		}

		se (altura1 == altura2) {
			escreva("Os dois tem o mesmo peso\n")
		} senao {
			escreva("Com ", maior_altura, "m o mais alta é: ", nome_mais_alta, "\n")	
		}
		
		escreva("===================\n")
		
	}
}
