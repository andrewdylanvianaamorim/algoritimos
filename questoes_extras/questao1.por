programa
{
	
	funcao inicio()
	{
		const inteiro FLAG = 1111
		inteiro num, somatorio = 0
		faca {
			escreva("Digite um número ou '1111' - para sair: ")
			leia(num)
			somatorio += num
		} enquanto (num != FLAG)
		somatorio -= FLAG
		escreva("Somatório: ", somatorio)
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 275; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */