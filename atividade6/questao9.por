programa {
	funcao inicio() {
		inteiro valor_saque

		escreva("Digite o valor do saque: ")
		leia(valor_saque)

		se (valor_saque < 1) {
			escreva("Valor inválido!\n")
		} senao {
			inteiro qtd_notas_100 = valor_saque / 100
			valor_saque = valor_saque % 100
			inteiro qtd_notas_10 = valor_saque / 10
			valor_saque = valor_saque % 10
			inteiro qtd_notas_5 = valor_saque / 5
			valor_saque = valor_saque % 5
			inteiro qtd_notas_2 = valor_saque / 2
			valor_saque = valor_saque % 2
			inteiro qtd_notas_1 = valor_saque

			escreva("Quantidade de notas de 100: ", qtd_notas_100, "\n")
			escreva("Quantidade de notas de 10: ", qtd_notas_10, "\n")
			escreva("Quantidade de notas de 5: ", qtd_notas_5, "\n")
			escreva("Quantidade de notas de 2: ", qtd_notas_2, "\n")
			escreva("Quantidade de notas de 1: ", qtd_notas_1, "\n")
			
		}
	}
}
