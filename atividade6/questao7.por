programa {
	funcao inicio() {
		const inteiro TOTAL_DE_ALUNOS = 5
		real altura
		inteiro idade
		real somatorio_idade_alunos_menores_que_1_70 = 0
		real somatorio_altura_alunos_maiores_de_20_anos = 0
		real idade_media_dos_menos_que_1_70
		real altura_media_dos_alunos_com_mais_de_20_anos
		inteiro qtd_menores_que_1_70 = 0
		inteiro qtd_maiores_que_20_anos = 0

		para (inteiro i = 1; i < TOTAL_DE_ALUNOS + 1; i++) {
			escreva("Digite a altura do ", i, "º aluno: ")
			leia(altura)
			escreva("Digite a idade do ", i, "º aluno: ")
			leia(idade)
			
			se (idade > 20) {
				qtd_maiores_que_20_anos++
				somatorio_altura_alunos_maiores_de_20_anos += altura
			}

			se (altura < 1.70) {
				qtd_menores_que_1_70++
				somatorio_idade_alunos_menores_que_1_70 += idade
			}			
		}

		altura_media_dos_alunos_com_mais_de_20_anos = somatorio_altura_alunos_maiores_de_20_anos / qtd_maiores_que_20_anos

		idade_media_dos_menos_que_1_70 = somatorio_idade_alunos_menores_que_1_70 / qtd_menores_que_1_70

		escreva("Altura média dos que são maiores do que 20 anos: ", altura_media_dos_alunos_com_mais_de_20_anos, "\n")
		escreva("Idade média dos que são menores que 1,70m: ", idade_media_dos_menos_que_1_70)
		
	}
}
