programa {
	funcao inicio() {
		escreva("Digite o valor inicial: ")
		inteiro comeco
		leia(comeco)
		escreva("Digite o valor final: ")
		inteiro final
		leia(final)
		escreva("Digite seu incremento: ")
		inteiro incremento
		leia(incremento)
		
		para (inteiro i = comeco; i < final + 1; i += incremento) {
			escreva(i, " ")
		}
		escreva("Acabou\n")
	}
}
