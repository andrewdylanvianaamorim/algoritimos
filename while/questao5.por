// Essa qustão é ambígua no sentido de poder incluir ou não o final na sequência
// Assumirei que o final não faz parte da sequência


programa
{
	
	funcao inicio()
	{
		escreva("INICIO: ")
		inteiro inicio
		leia(inicio)
		escreva("FIM: ")
		inteiro fim
		leia(fim)
		escreva("PASSO: ")
		inteiro passo
		leia(passo)
		inteiro contador = inicio
		escreva("Contagem: ")
		se (passo == 0) passo++
		se (inicio > fim e passo > 0) passo *= -1
		enquanto (verdadeiro) {
			se (passo > 0 e contador > fim - 1) pare
			senao se (passo < 0 e contador - 1 < fim) pare
			escreva(contador, ' ')
			contador += passo
		}
		escreva("Acabou!\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 559; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */