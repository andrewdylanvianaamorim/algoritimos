programa {
    real SALDO = 0.0

    logico CONTINUAR = verdadeiro
 
    funcao EsperaEnter() {
	escreva("Digite <ENTER> para continuar...")
	cadeia c
	leia(c)
    }

    funcao Deposito(real vlr_deposito) {
        SALDO += vlr_deposito
    }

    funcao Saque(real vlr_saque) {
        se (SALDO - vlr_saque > 0)
            SALDO -= vlr_saque
        senao
            escreva("Saldo insuficiente!\n")
	    EsperaEnter()
    }

    funcao real VerSaldo() {
        retorne SALDO
    }

    funcao DesenhaMenu() {
        limpa()
        escreva("----------------------\n")
        escreva("\tBanco IFPI\n")
        escreva("----------------------\n")
        escreva("[1] - Deposito\n")
        escreva("[2] - Saque\n")
        escreva("[3] - Ver Saldo\n")
        escreva("[4] - Sair\n")
        escreva("----------------------\n")
        escreva("Digite a opção que você deseja: ")
    }
   
    funcao LidaComOMenu() {
        inteiro opcao
        leia(opcao)
	limpa()
        escolha(opcao) {
            caso 1:
                escreva("Digite o valor do Desposito: ")
                real deposito
                leia(deposito)
                Deposito(deposito)
                pare
            caso 2:
                escreva("Digite o valor do Saque: ")
                real vlr_saque
		leia(vlr_saque)
                Saque(vlr_saque)
                pare
            caso 3:
		escreva("Saldo: R$", VerSaldo(), "\n")
		EsperaEnter()
		pare
	    caso 4:
		escreva("saindo...")
		CONTINUAR = falso
        }
    }

    funcao inicio() {
	faca {
	    DesenhaMenu()
	    LidaComOMenu()
	} enquanto (CONTINUAR)

    }
}
