programa {
	funcao inicio() {
		const real TAXA_FIXA_MENOR_QUE_11 = 36.21
		const real TAXA_FIXA_11_15 = 36.21
		const real TAXA_FIXA_16_20 = 69.96
		const real TAXA_FIXA_21_25 = 107.06
		const real TAXA_FIXA_26_35 = 147.56
		const real TAXA_FIXA_MAIOR_QUE_35 = 275.66

		const real PRECO_METRO_CUBICO_11_15 = 6.75
		const real PRECO_METRO_CUBICO_16_20 = 7.42
		const real PRECO_METRO_CUBICO_21_25 = 8.10
		const real PRECO_METRO_CUBICO_26_35 = 12.81
		const real PRECO_METRO_CUBICO_MAIOR_QUE_35 = 13.98

		real metros_cubicos, total_a_pagar
		
		escreva("Digite a quantidade de metros cúbicos gastos: ")
		leia(metros_cubicos)

		se (metros_cubicos < 11) {
			total_a_pagar = TAXA_FIXA_MENOR_QUE_11
		} senao se (metros_cubicos < 16) {
			total_a_pagar = TAXA_FIXA_11_15 * PRECO_METRO_CUBICO_11_15
		} senao se (metros_cubicos < 21) {
			total_a_pagar = TAXA_FIXA_16_20 * PRECO_METRO_CUBICO_16_20
		} senao se (metros_cubicos < 26) {
			total_a_pagar = TAXA_FIXA_21_25 * PRECO_METRO_CUBICO_21_25
		} senao se (metros_cubicos < 36) {
			total_a_pagar = TAXA_FIXA_26_35 * PRECO_METRO_CUBICO_26_35
		} senao {
			total_a_pagar = TAXA_FIXA_MAIOR_QUE_35 * PRECO_METRO_CUBICO_MAIOR_QUE_35
		}

		escreva("Você pagará R$", total_a_pagar)
		
	}
}
