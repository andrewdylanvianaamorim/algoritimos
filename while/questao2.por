programa
{
	
	funcao inicio()
	{
		caracter continuar = 's'
		inteiro qtd_idades = 0
		inteiro qtd_maiores_que_20 = 0
		real somatorio_idade = 0
		real idade = 0


		faca {
			escreva("Digite a idade da ", qtd_idades + 1,"º pessoa: ")
			leia(idade)
			qtd_idades++
			se (idade > 20) {
				qtd_maiores_que_20++	
			}
			somatorio_idade += idade
			escreva("Deja digitar outra pessoa[s\\n]: ")
			leia(continuar)
		} enquanto (continuar != 'n' e continuar != 'N')


		real media = somatorio_idade / qtd_idades

		escreva("Você digitou aidade de ", qtd_idades, " pessoas\n")
		escreva("média das idades: ", media,'\n')
		escreva("quantidade de pessoas maiores que 20 anos: ", qtd_maiores_que_20,'\n')
		
	}
}

/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 708; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */