programa {
    funcao contador(inteiro inicio, inteiro fim, inteiro incremento) {
	se (incremento == 0) incremento++
	se ((inicio > fim e incremento > 0) ou (fim < inicio e incremento < 0)) incremento *= -1
	para (inteiro c = inicio; c <= fim; c += incremento) escreva(c, " >> ")
	escreva("FIM\n")
    }

    funcao inicio() {
	contador(4,20,3)
    }
}
