programa {
    //retorna 0 se não houver errors
    funcao inteiro zera(inteiro vetor[], inteiro elemento, inteiro tamanho) {
        para(inteiro c = 0; c < tamanho; c++) {
            se (elemento == vetor[c]) {
                vetor[c] = 0;
                retorne 0;
            }
        }
        retorne -1
    }

    funcao inicio() {
    
    }
}
