programa {
   
    funcao inicio() {
        const inteiro TAMANHO = 3
        inteiro v[TAMANHO], v2[TAMANHO], resultado[TAMANHO]
        escreva("Digites os valores do 1° vetor: \n")
        para (inteiro i = 0; i < TAMANHO; i++) {
            escreva("Digite um valor: ")
            leia(v[i])
        }
        escreva("Digites os valores do 2° vetor: \n")
        para (inteiro i = 0; i < TAMANHO; i++) {
            escreva("Digite um valor: ")
            leia(v2[i])
        }

        para (inteiro i = 0; i < TAMANHO; i++) {
            resultado[i] = v[i] + v2[i]
        }

    }
}
