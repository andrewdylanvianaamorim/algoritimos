programa {
	funcao inicio() {
		const real PRECO_DIARIA = 120.0
		const real TAXA_DE_SERVICO_PARA_MAIOR_QUE_15_DIAS = 5.50
		const real TAXA_DE_SERVICO_PARA_MENOR_QUE_15_DIAS = 8.0
		const real TAXA_DE_SERVICO_PARA_15_DIAS = 6.0

		cadeia nome_cliente
		inteiro qtd_de_diarias = 0 //Este valor será usado posteriormente
		real total

		escreva("Digite o nome do cliente: ")
		leia(nome_cliente)

		escreva("Digite a quantidade de diarias: ")
		leia(qtd_de_diarias)

		se (qtd_de_diarias < 1) {
			escreva("O quantidade de diárias digitada é inválida. Tente novamente")
		} senao {
			se (qtd_de_diarias < 15) {
				total = PRECO_DIARIA * TAXA_DE_SERVICO_PARA_MENOR_QUE_15_DIAS
			} senao se (qtd_de_diarias > 15) {
				total = PRECO_DIARIA * TAXA_DE_SERVICO_PARA_MAIOR_QUE_15_DIAS
			} senao {
				total = PRECO_DIARIA * TAXA_DE_SERVICO_PARA_15_DIAS
			}

			escreva("A conta do cliente ",nome_cliente," deu R$", total)
		}
	}
}
