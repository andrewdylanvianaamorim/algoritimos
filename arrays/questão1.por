programa {
    funcao inicio() {
        const inteiro TAMANHO = 7
        inteiro v[TAMANHO]
        para(inteiro c = 0; c < TAMANHO; c++) {
            escreva("Digite um número: ")
            leia(v[c])
        }
    }
}
