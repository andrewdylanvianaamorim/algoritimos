programa
{
	
	funcao inicio()
	{
		const inteiro NUMERO_MAGICO = 42
		inteiro tentativas = 0
		inteiro palpite = 0
		inteiro max=100, min=0
		
		
		faca {
			escreva("Digite um número entre ", min, " e ", max,":")
			leia(palpite)
			se (palpite < max e palpite >= NUMERO_MAGICO) max = palpite
			se (palpite > min e palpite <= NUMERO_MAGICO) min = palpite
			tentativas++
		} enquanto (palpite != NUMERO_MAGICO) 
		escreva("Parabéns você acertou o número em ", tentativas, " tentativa(s)!\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 147; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */