programa
{
	
	funcao inicio()
	{
		inteiro valor = 0
		inteiro menor_valor = 0
		inteiro maior_valor = 0
		real somatorio = 0.0
		real qtd_valores = 0.0
		inteiro qtd_pares = 0
		caracter continuar = '\0' // \0 é NULL na tabela ascii e não tem como entara com esse caractere no portugol
		faca {
			escreva("Digite um valor: ")
			leia(valor)
			qtd_valores++
			somatorio += valor
			se (valor % 2 == 0)
				qtd_pares++
			se (continuar == '\0') {
				menor_valor = valor
				maior_valor = valor
			}
			senao {
				se (valor < menor_valor) menor_valor = valor
				se (valor > maior_valor) maior_valor = valor	
			}
			escreva("Deseja continuar a inserir valores? [s/n]: ")
			leia(continuar)
		} enquanto(continuar != 'n' e continuar != 'N')

		real media = somatorio / qtd_valores

		escreva("Somatório: ", somatorio, "\n")
		escreva("Maior valor: ", maior_valor, "\n")
		escreva("Menor valor: ", menor_valor, "\n")
		escreva("Média: ", media, "\n")
		escreva("Quantidade de Pares: ", qtd_pares, "\n")

		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1006; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */