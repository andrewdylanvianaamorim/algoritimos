programa {
    funcao inteiro SuperSomador(inteiro inicio, inteiro fim) {
	inteiro resultado = 0
	para (inteiro c = inicio; c < fim + 1; c++) resultado += c
	retorne resultado
    }
    funcao inicio() {
	escreva(SuperSomador(1,6), "\n")
	escreva(SuperSomador(15, 19))
    }
}
