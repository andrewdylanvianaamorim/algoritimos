programa {
	funcao inicio() {
		escreva("Digite sua idade: ")
		inteiro idade
		leia(idade)

		cadeia categoria = ""
		se (idade < 8) {
			categoria = "Infantil"
		} senao se (idade < 16) {
			categoria = "Juvenil"
		} senao se (idade < 51) {
			categoria = "Adulto"
		} senao {
			categoria = "Sênior"
		}

		escreva("Você está na categoria: ", categoria)
	}
}
