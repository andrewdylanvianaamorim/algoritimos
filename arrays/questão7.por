programa {
    const inteiro ERRO = -1
    const inteiro OK = 1

    funcao inteiro cadastrar_clientes(cadeia nomes[], real saldos[], inteiro qtd_clientes, inteiro maximo_de_clientes) {
        se (qtd_clientes  == maximo_de_clientes) retorne ERRO
        inteiro cliente_atual = qtd_clientes
        escreva("Digite o nome do cliente: ")
        leia(nomes[cliente_atual])
        saldos[cliente_atual] = 0.0
        retorne qtd_clientes + 1
    }

    funcao inteiro pega_usuario_por_nome(cadeia nome, cadeia nomes[], inteiro maximo_de_clientes) {
        inteiro usuario = ERRO
        para (inteiro i = 0; i < maximo_de_clientes; i++) {
            se (nomes[i] == nome) usuario = i
        }
        retorne usuario
    }

    funcao inteiro deposito(cadeia nome, real vlr_deposito, cadeia nomes[], real saldos[], inteiro maximo_de_clientes) {
        inteiro usuario = pega_usuario_por_nome(nome, nomes, maximo_de_clientes)
        se (usuario == ERRO) retorne ERRO
        saldos[usuario] += vlr_deposito
        retorne OK
    }

    funcao inteiro saque(cadeia nome, real vlr_saque, cadeia nomes[], real saldos[], inteiro maximo_de_clientes) {
        inteiro usuario = pega_usuario_por_nome(nome, nomes, maximo_de_clientes)
        se (usuario == ERRO) retorne ERRO
        se (saldos[usuario] - vlr_saque < 0) retorne ERRO
        saldos[usuario] -= vlr_saque
        retorne OK
    }

    funcao inteiro exibir_saldo(cadeia nome, cadeia nomes[], real saldos[], inteiro maximo_de_clientes) {
        inteiro usuario = pega_usuario_por_nome(nome, nomes, maximo_de_clientes)
        se (usuario == ERRO) retorne ERRO
        escreva("O saldo"," de ",nomes[usuario]," é", ": R$", saldos[usuario], "\n")
        retorne OK
    }

    funcao desenha_menu() {
       limpa()
       escreva("==========MENU=========\n")
       escreva("[1] - para cadastrar um novo usuario\n")
       escreva("[2] - para realizar um deposito\n")
       escreva("[3] - para realizar um saque\n")
       escreva("[4] - para visualizar o saldo\n")
       escreva("[5] - para sair\n")
       escreva("=======================\n")
       escreva("Digite a opção desejada: ")
    }

    funcao espera_enter() {
        escreva("Digite <ENTER> para continuar...")
        cadeia c
        leia(c)
    }

    funcao inicio() {
        logico continuar = verdadeiro
        inteiro op, resultado
        inteiro qtd_clientes = 0
        const inteiro MAX_CLIENTES = 2
        cadeia nome, nomes[MAX_CLIENTES]
        real saldos[MAX_CLIENTES]
        real vlr_deposito, vlr_saque
        
        faca {
            desenha_menu()
            leia(op)
            escolha(op) {
                caso 5:
                    escreva("Saindo...\n")
                    continuar = falso
                    pare
                caso 1:
                    resultado = cadastrar_clientes(nomes, saldos, qtd_clientes, MAX_CLIENTES)
                    se(resultado == ERRO) {
                        escreva("Não foi possível cadastrar esse usuário!\n")
                    } senao {
                        qtd_clientes = resultado
                        escreva("O cliente '", nomes[qtd_clientes-1], "' foi cadastrado com sucesso!\n")
                    }
                    espera_enter()
                    pare
                //Isso não é um bug é intencional (Eu quero o fallthrough)
                caso 2:
                caso 3:
                caso 4:
                    escreva("Digite o nome do cliente: ")
                    leia(nome)
                    escolha(op) {
                        caso 2:
                            escreva("Digite o valor do deposito: ")
                            leia(vlr_deposito)
                            resultado = deposito(nome, vlr_deposito, nomes, saldos, MAX_CLIENTES)
                            se (resultado == ERRO) {
                                escreva("Erro: Conta inexistente!\n")
                            } senao {
                                escreva("Valor depositado com sucesso!\n")
                            }
                            pare
                        caso 3:
                            escreva("Digite o valor do saque: ")
                            leia(vlr_saque)
                            resultado = saque(nome, vlr_saque, nomes, saldos, MAX_CLIENTES)
                            se (resultado == ERRO) {
                                se (pega_usuario_por_nome(nome, nomes, MAX_CLIENTES) == ERRO) {
                                    escreva("Erro: Conta inexistente!\n")
                                } senao {
                                    escreva("Erro: Saldo insuficiente!\n")
                                }
                            } senao {
                                escreva("Saque realizado com sucesso!\n")
                            }
                            pare
                        caso 4:
                            se (exibir_saldo(nome, nomes, saldos, MAX_CLIENTES) == ERRO) {
                                escreva("Erro: Conta inexistente!\n")
                            }
                            pare
                    }
                    espera_enter()
                    pare
            }
        } enquanto(continuar)
    }
}