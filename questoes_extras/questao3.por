programa
{
	
	funcao inicio()
	{
		escreva("Digite o número maior que zero que você deseja ver o fatorial: ")
		inteiro num
		leia(num)
		inteiro resultado = 1
		para (inteiro i = 2; i < num + 1; i++) resultado *= i
		escreva(num,"! = ", resultado, "\n")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 269; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */