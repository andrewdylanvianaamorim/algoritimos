// Essa qustão é ambígua no sentido de poder incluir ou não o final na sequência
// Assumirei que o final não faz parte da sequência


programa
{
	
	funcao inicio()
	{
		escreva("INICIO: ")
		inteiro inicio
		leia(inicio)
		escreva("FIM: ")
		inteiro fim
		leia(fim)
		escreva("PASSO: ")
		inteiro passo
		leia(passo)
		inteiro contador = inicio
		escreva("Contagem: ")
		se (passo == 0) passo++
		enquanto (contador < fim) {
			escreva(contador, ' ')
			contador += passo
		}
		escreva("Acabou!\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 400; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */