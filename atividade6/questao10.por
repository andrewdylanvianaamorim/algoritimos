programa {
	funcao inicio() {
		inteiro i

		escreva("Digite um número: ")
		leia(i)

		inteiro contador = 0
		para (inteiro c = i; c > 1; c /= 2) {
			contador++
			// Esse 4 é pra fazer o loop ir além (mais ou menos)
			se ((c / 4) < 1){
				escreva(c," \\ 2 = ", c / 2, "\n")
			} 
		} 
		escreva("Foram feitas ", contador, " divisões")
		
	}
}
