programa {
    funcao inicio() {
        const inteiro TAMANHO = 20;
        const inteiro METADE = 10;
        inteiro v[TAMANHO];
        para(inteiro i = 0; i < TAMANHO; i++) {
            escreva("digite um valor: ")
            leia(v[i])
        }
        inteiro tmp
        para(inteiro i = 0; i < METADE; i++) {
            tmp = v[i]
            v[i] = v[(TAMANHO - 1) - i]
            v[(TAMANHO - 1) - i] = tmp
        }

        para(inteiro i = 0; i < TAMANHO; i++) {
            escreva(v[i]," ")
        }

        escreva("\n")
    }
}
