programa {
	funcao inicio() {
		real s = 0.0
		logico e_soma = verdadeiro
		para (real i = 1.0; i < 11.0; i++) {
			se (e_soma) {
				s += i / (i * i)
			} senao {
				s -= i / (i * i)
			}

			e_soma = nao e_soma
		}
		escreva("S = ", s)
	}
}
