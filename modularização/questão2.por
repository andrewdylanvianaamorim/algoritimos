programa {
    funcao ParOuImpar(inteiro x) {
	se (x % 2 == 0)
	    escreva(x, " é par!\n")
	senao
	    escreva(x, " é ímpar!\n")
    }
    
    funcao inicio() {
	escreva("Digite um número: ")
	inteiro num
	leia(num)
	ParOuImpar(num)
    }
}
