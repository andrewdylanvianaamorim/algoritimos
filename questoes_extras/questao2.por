programa
{
	
	funcao inicio()
	{
		const inteiro ANO_MIN = 2023
		const inteiro ANO_MAX = 2050
		const inteiro POPULACAO_ANO_2022 = 20000
		const real PORCENTAGEM_CRESCIMENTO = 5.0
		real ano = 0.0
		inteiro populacao = POPULACAO_ANO_2022
		
		faca {
			escreva("Digite um ano entre ", ANO_MIN, " e ", ANO_MAX, ":")
			leia(ano)
		} enquanto(ano < ANO_MIN ou ano > ANO_MAX)

		para (real i = 0.0; i < ano  - 2022; i++) populacao *= (1.0 + (PORCENTAGEM_CRESCIMENTO / 100.0))
		escreva("A população em ", ano, " é ", populacao, "!")
		
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 546; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */