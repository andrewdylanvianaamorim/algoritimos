programa {
    funcao Maior(inteiro a, inteiro b) {
	se (a == b)
	    escreva("Os dois número são iguais")
	senao se (a > b)
	    escreva(a, " é o maior número digitado!\n")
	senao
	    escreva(b, " é o maior número digitado!\n")
    }

    funcao inicio() {
	escreva("Digite um número: ")
	inteiro num
	leia(num)
	escreva("Digite outro número: ")
	inteiro num2
	leia(num2)
	Maior(num, num2)
    }
}
