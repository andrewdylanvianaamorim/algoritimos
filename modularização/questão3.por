programa {
    funcao real media(real nota, real nota2) {
	retorne (nota + nota2) / 2.0
    }

    funcao inicio() {
	escreva("Digite a primeira nota: ")
	real nota1
	leia(nota1)
	escreva("Digite a segunda nota: ")
	real nota2
	leia(nota2)
	escreva("A média é: ", media(nota1, nota2))
    }
}
