programa
{
	
	funcao inicio()
	{
		const inteiro FLAG = 1111
		inteiro somatorio = 0, num
		enquanto (verdadeiro) {
			escreva("Digite um número ou '1111' para sair: ")	
			leia(num)
			se (num == FLAG) pare
			somatorio += num
		}
		
		escreva("Somatório: ", somatorio, "\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 285; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */