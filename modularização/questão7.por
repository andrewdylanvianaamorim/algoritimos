programa {
	funcao Maior(inteiro a, inteiro b) {
		se (a == b)
			escreva("Os dois número são iguais")
		senao se (a > b)
			escreva(a, " é o maior número digitado!\n")
		senao
			escreva(b, " é o maior número digitado!\n")
    }

    funcao inteiro SuperSomador(inteiro inicio, inteiro fim) {
		inteiro resultado = 0
		para (inteiro c = inicio; c < fim + 1; c++) resultado += c
		retorne resultado
    }

    funcao inteiro Potencia(inteiro base, inteiro expoente) {
        inteiro resultado = base
        para(inteiro c = 1; c < expoente; c++) resultado *= base
        retorne resultado
    }

    funcao Contador(inteiro inicio, inteiro fim, inteiro incremento) {
		se (incremento == 0) incremento++
		se ((inicio > fim e incremento > 0) ou (fim < inicio e incremento < 0)) incremento *= -1
		para (inteiro c = inicio; c <= fim; c += incremento) escreva(c, " >> ")
		escreva("FIM\n")
    }

    funcao ParOuImpar(inteiro x) {
	se (x % 2 == 0)
	    escreva(x, " é par!\n")
	senao
	    escreva(x, " é ímpar!\n")
    }

    funcao real Media(real nota, real nota2) {
	retorne (nota + nota2) / 2.0
    }



    funcao inteiro Menu() {
		escreva("-----------------------\n")
		escreva("\tMENU DE OPÇÔES\n")
		escreva("-----------------------\n")
		escreva("1 - Maior\n")
		escreva("2 - Par ou Ímpar\n")
		escreva("3 - Média\n")
		escreva("4 - Contador\n")
		escreva("5 - Potencia\n")
		escreva("6 - Super Somador\n")
		escreva("7 - Sair\n")
		escreva("-----------------------\n")
		escreva(" Opção: ")
		inteiro opcao
		leia(opcao)
		retorne opcao
    }

    funcao ChamaFuncaoCorreta(inteiro opcao) {
		se (opcao == 7) retorne

		real arg1, arg2, arg3
		arg1 = 0.0
		arg2 = 0.0
		arg3 = 0.0

		escreva("Digite o primeiro argumento: ")
		leia(arg1)
		escolha (opcao) {
			caso 1:
				escreva("Digite o segundo argumento: ")
				leia(arg2)
				escreva("Executando: Maior(",arg1, ", ", arg2, ")\n")
				Maior(arg1, arg2)
				pare
			caso 2:
				escreva("Executando: ParOuImpar(",arg1,")\n")
				ParOuImpar(arg1)
				pare
			caso 3:
				escreva("Digite o segundo argumento: ")
				leia(arg2)
				escreva("Executando: Media(",arg1, ", ", arg2, ")\n")
				escreva("Resultado = ", Media(arg1, arg2), "\n")
				pare
			caso 4:
				escreva("Digite o segundo argumento: ")
				leia(arg2)
				escreva("Digite o terceiro argumento: ")
				leia(arg3)
				escreva("Executando: Contador(", arg1, ", ", arg2, ", ", arg3, ")\n")
				Contador(arg1,arg2,arg3)
				pare
			caso 5:
				escreva("Digite o segundo argumento: ")
				leia(arg2)
				escreva("Executando: Potencia(",arg1, ", ", arg2, ")\n")
				escreva("Resultado = ", Potencia(arg1, arg2), "\n")
				pare
			caso 6:
				escreva("Digite o segundo argumento: ")
				leia(arg2)
				escreva("Executando: SuperSomador(",arg1, ", ", arg2, ")\n")
				escreva("Resultado = ", SuperSomador(arg1, arg2), "\n")
				pare
		}
	}

    funcao inicio() {
		inteiro opcao = 0
		faca {
			opcao = Menu()
			ChamaFuncaoCorreta(opcao)
		} enquanto (opcao != 7)
		escreva("saindo...")
    }
}
