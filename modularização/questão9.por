programa {
    inteiro ESTOQUE = 750
    const inteiro ESTOQUE_MINIMO = 500
    const inteiro SAIR = 0
    const inteiro CONTINUAR = 1

    funcao esperaEnter() {
	escreva("Digite <ENTER> para continuar...")
	cadeia c
	leia(c)
    }


    funcao cadastrar() {
	escreva("Digite a quantidade de produto a ser cadastrado: ")
	inteiro qtd
	leia(qtd)
	ESTOQUE += qtd
	esperaEnter()
    }

    funcao vender() {
	se (ESTOQUE < ESTOQUE_MINIMO + 1)
	escreva("AVISO: O ESTOQUE ATINGIU SEU MÍNIMO.\n")
	escreva("Digite a quantidade da venda: ")
	inteiro qtd
	leia(qtd)
	se (ESTOQUE < qtd) {
	    escreva("Não foi possível efetuar a venda...\n")
	    esperaEnter()
	    retorne
	}
	ESTOQUE -= qtd
	escreva("Venda bem sucedida!\n")
	esperaEnter()
    }


    
    funcao desenhaMenu() {
	escreva("Menu:\n")
	escreva("\t[1] - Cadastrar\n")
	escreva("\t[2] - Vender\n")
	escreva("\t[3] - Ver Estoque\n")
	escreva("\t[4] - Sair\n")
	escreva("Digite sua opção: ")
    }

    funcao inteiro verEstoque() {
	retorne ESTOQUE
    }

    funcao inteiro lidaComOMenu() {
	inteiro op
	leia(op)
	limpa()
	escolha(op) {
	    caso 1:
		cadastrar()
		pare
	    caso 2:
		vender()
		pare
	    caso 3:
		escreva("ESTOQUE: ", verEstoque(), "\n")
		esperaEnter()
		pare
	    caso 4:
		retorne SAIR
	}
    
	retorne CONTINUAR
    }
    
    funcao inicio() {
	inteiro i
	faca {
	    desenhaMenu()
	    i = lidaComOMenu()
	} enquanto(i != SAIR)
    }
}
