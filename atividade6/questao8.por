programa {
	funcao inicio() {
		inteiro operacao, numero_da_conta
		real valor_da_operacao, saldo, novo_saldo

		escreva("Digite o número da conta: ")
		leia(numero_da_conta)

		escreva("Digite a operção desejada [1 - deposito | 2 - saque]: ")
		leia(operacao)

		escreva("Digite o saldo: ")
		leia(saldo)

		se (operacao != 1 e operacao != 2) {
			escreva("Operação inválida.Tente novamente\n")
		} senao {
			escreva("Digite o valor da operação: ")
			leia(valor_da_operacao)

			se (valor_da_operacao < 0.0) {
				escreva("Valor de operação inválido. Prosseguindo com o valor de zero\n")
				valor_da_operacao = 0
			}

			se (operacao == 1) {
				novo_saldo = saldo + valor_da_operacao
			} senao {
				novo_saldo = saldo - valor_da_operacao
			}

			escreva("O novo saldo é: R$", novo_saldo, "\n")

			se (novo_saldo < 0.0) {
				escreva("CONTA ESTORADA")
			}
		}
		
	}
}
