programa {
	funcao inicio() {

		const real FATOR_METROS_CM = 100.0

		escreva("Digite seu sexo: ")
		caracter sexo
		leia(sexo)
		escreva("Qual a sua altura(em metros): ")
		real altura
		leia(altura)
		//transformando metros em cm
		altura *= FATOR_METROS_CM
	
		real peso_ideal = 0.0
		se (sexo == 'M' ou sexo == 'm') {
			peso_ideal = altura * 0.95 - 95
		} senao se (sexo == 'F' ou sexo == 'f') {
			peso_ideal = altura * 0.85 - 85
		}

		se (peso_ideal == 0.0) {
			escreva("Um erro acontesceu, verifique os dados entradaos\n")
			escreva("E tente novamente!")
		} senao {
			escreva("Seu peso ideal é: ", peso_ideal)
		}
	}
}
