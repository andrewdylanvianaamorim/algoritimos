programa
{
	
	funcao inicio()
	{
		const caracter FLAG = 'x'
		inteiro idade
		caracter sexo
		real qtd_homens = 0.0
		inteiro idade_mulher_mais_jovem = -1
		real somatorio_homens = 0.0
		real media_dos_homens = 0.0
		inteiro maior_idade = -1

		enquanto(verdadeiro) {
			escreva("Digite o sexo [m/f ou 'x' para sair]: ")
			leia(sexo)
			se (sexo != 'm' e sexo != 'M' e sexo != 'f' e sexo != 'F' e sexo != 'x' e sexo != 'X') {
				escreva("Sexo inválido! Tente novamente!")
			} senao {
				se (sexo == 'x' ou sexo == 'X') pare
				escreva("Digite a idade: ")
				leia(idade)
				se (idade < 1) {
					escreva("Idadde inválida! Tente Novamente!")	
				}  senao {
					se (maior_idade == -1) maior_idade = idade
					senao se (idade > maior_idade) maior_idade = idade
					se (sexo == 'f' ou sexo == 'F') {
						se (idade_mulher_mais_jovem == -1) idade_mulher_mais_jovem = idade
						senao se (idade_mulher_mais_jovem < idade) idade_mulher_mais_jovem = idade
					} senao {
						qtd_homens++
						somatorio_homens += idade
					}
				}
			}
		}
		media_dos_homens = somatorio_homens / qtd_homens

		escreva("Maior idade: ", maior_idade, "\n")
		escreva("Quantidade de homens: ", qtd_homens, "\n")
		escreva("Idade da mulher mais jovem: ", idade_mulher_mais_jovem, "\n")
		escreva("Media de idade entre os homens: ", media_dos_homens, "\n")
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1346; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */