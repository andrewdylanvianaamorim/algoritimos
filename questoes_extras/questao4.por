programa
{	
	funcao inicio()
	{
		real totalCompra = 0.0
		real totalApurado = 0.0
		real precoProduto = 0.0
		inteiro qtdProduto = 0
		caracter opcao
		logico caixaDeveSerFechado = falso
		logico continuarNaCompra = verdadeiro
		faca {
			escreva("========MENU=========\n")
			escreva("[1] - para uma nova  compra\n")
			escreva("[2] - para fechar o caixa\n")
			escreva("========MENU=========\n")
			escreva("Digite sua opção: ")
			leia(opcao)
			se (opcao == '1') {
				//conta quantos items foram comprados
				inteiro contador = 1
				faca {
					
					escreva("==========",contador,"º ITEM==========\n")
					escreva("Digite o preço do ", contador, "º item: ")
					leia(precoProduto)
					escreva("Digite a quantidade do ", contador, "º item: ")
					leia(qtdProduto)
					//evitando quantidades negativas
					se (qtdProduto < 1) qtdProduto = 1
					totalCompra += precoProduto * qtdProduto
					escreva("======================================\n")
					escreva("Deseja continuar a adicionar itens compra? [s/n]: ")
					leia(opcao)
					se (opcao == 'n' ou  opcao == 'N') continuarNaCompra = falso
					contador++
				} enquanto (continuarNaCompra)
				totalApurado += totalCompra
				escreva("Total dessa compra: R$", totalCompra, "\n")
				totalCompra = 0.0
				continuarNaCompra = verdadeiro
			
			} senao se (opcao == '2') {
					caixaDeveSerFechado = verdadeiro
					escreva("==========APURACÃO==========\n")
					escreva("Valor total apurado: R$", totalApurado,"\n")
					escreva("============================\n")
					escreva("SAINDO DO PROGRAMA...")
					
			} senao {
				escreva("Você digitou uma opção inválida! Tente Novamente\n")
			}
		} enquanto (nao caixaDeveSerFechado)
	}
}
/* $$$ Portugol Studio $$$ 
 * 
 * Esta seção do arquivo guarda informações do Portugol Studio.
 * Você pode apagá-la se estiver utilizando outro editor.
 * 
 * @POSICAO-CURSOR = 1127; 
 * @PONTOS-DE-PARADA = ;
 * @SIMBOLOS-INSPECIONADOS = ;
 * @FILTRO-ARVORE-TIPOS-DE-DADO = inteiro, real, logico, cadeia, caracter, vazio;
 * @FILTRO-ARVORE-TIPOS-DE-SIMBOLO = variavel, vetor, matriz, funcao;
 */